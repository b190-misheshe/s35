const express = require("express");
const mongoose = require("mongoose");

const app = express();

mongoose.connect("mongodb+srv://misheshe:Wanderviz@wdc028-course-booking.jj30hwt.mongodb.net/b190-to-do?retryWrites=true&w=majority",
{
  useNewUrlParser: true,
  useUnifiedTopology: true
}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open",()=> console.log("We are connected to the database."));

// SECTION - Schema
// Schema() is a method inside the mongoose module that lets us create schema for our database that receives an object with properties and data types that each property should have
// const taskSchema = new mongoose.Schema ({
//   // the "name" property should receive a string data type (the data types should be written in Sentence case)
//   name: String,
//   status: {
//     type: String, 
//     default: "pending"
//   }
// });

// SECTION - Models
// models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use Schemas and they act as the middleman from the server to the database
// First parameter is the collection in where to store the data
// Second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

// const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Create a new task
/*
  BUSINESS LOGIC
    -add a functionality that will check if there are duplicate tasks
      -if the task is already existing, we return an error
      -if task is not existing, we add it in the database

    -the task wil be sent form the request body

    -create a new Task object with a "name" field/property
    -the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
// creating a to-do (post)
// app.post("/tasks", (req,res) => {

//   Task.findOne({name:req.body.name},(err, result) => {
//     if (result !== null && result.name === req.body.name) {
//       return result.send("Duplicate task found");
//     } else {
//       let newTask = new Task ({
//         name:req.body.name
//       });
//       newTask.save((saveErr, savedTask) => {
//         if (saveErr){
//           return console.log(saveErr);
//         } else {
//           return result.status(201).send("New task created");
//         };
//       });
//     };
//   }); 
// });


// getting all task (get)
// app.get("/tasks", (req,res) => {

//   Task.find({},(err, result) => {
//     if (err) {
//       return console.log(err);
//     } else {
//         return res.status(200).json();



//       let newTask = new Task ({
//         name:req.body.name
//       });
//       newTask.save((saveErr, savedTask) => {
//         if (saveErr){
//           return console.log(saveErr);
//         } else {
//           return result.status(201).send("New task created");
//         };
//       });
//     };
//   }); 
// });

// SECTION: ACTIVITY

/*
  Business Logic for registering a new user
    - add a functionality to check if there are duplicate users
      - if the user is already existing, return an error
      - if the user is not existing, save/add the user in the database

    - the user will be coming from the request's body

    - create a new User object with a "username" and "password" field password
*/
const userSchema = new mongoose.Schema ({
  username: String,
  password: {
    type: String
  }
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req,res) => {
  
  User.findOne({username:req.body.username},(err, result) => {
    if (result !== null && result.username === req.body.username) {
      return res.send("Duplicate user found");
    } else {
      let newUser = new User({
        username:req.body.username,
        password:req.body.password
      });
      newUser.save((saveErr, savedUser) => {
        if (saveErr){
          return console.error(saveErr);
        } else {
          return res.status(201).send("New user registered");
        };
      });
    };
  }); 
});

// let newUser = {
//   name: "john",
//   password: "johndoe"
// }

app.listen(3000, () => console.log('Server is listening on port 3000'));

// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.



